import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger);

function init() {

    // Get my sections as an Array of Elements
    let sections = [].slice.call(document.querySelectorAll('article'));
    
    // Loop through the array
    sections.forEach(section => {

        // Create new ScrollTrigger
        ScrollTrigger.create({

            trigger: section, // the Element
            start: 'top 20%', // 20% from the top of the page.
            toggleClass: 'animated' // the class to animate it.

        });

    });
}

// Wait until everything is loaded before running the function
document.addEventListener('DOMContentLoaded', init, false);