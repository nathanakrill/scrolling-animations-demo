# Animation Demo

Set up in index.html and style.css is a basic 3 section page, where each section has an entry animation, toggled with the class `animated`.

In main.js I have used [GSAP's ScrollTrigger plugin](https://greensock.com/docs/v3/Plugins/ScrollTrigger) to toggle the class on scroll.