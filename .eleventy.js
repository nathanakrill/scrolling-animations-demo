module.exports = function(eleventyConfig) {

    eleventyConfig.addPassthroughCopy('dist');
    eleventyConfig.addPassthroughCopy('img');
    eleventyConfig.addPassthroughCopy('style.css');
}